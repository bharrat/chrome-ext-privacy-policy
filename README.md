# Privacy Policy for Chrome Extensions  #
This policy applies to chrome extensions:
* CastBuddy
* QualysAPIExplorer
* JiraEnhancer

Above mentioned extensions are local to installed system and do not collect any personal information.
